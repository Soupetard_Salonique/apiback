var mongoose = require('mongoose');

// Setup schema
var postSchema = mongoose.Schema({
    title : String,
    desc: String,
    image: String
});

// Export post model
var Post = module.exports = mongoose.model('post', postSchema);

module.exports.get = function (callback, limit) {
    Post.find(callback).limit(limit);
}
